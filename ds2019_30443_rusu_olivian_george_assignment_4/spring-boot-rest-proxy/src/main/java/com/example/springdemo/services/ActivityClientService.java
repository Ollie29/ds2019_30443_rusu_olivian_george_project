package com.example.springdemo.services;

import com.example.springdemo.dto.ActivityDTO;
import com.example.springdemo.wsdl.Activity;
import com.example.springdemo.wsdl.GetActivitiesForPatientRequest;
import com.example.springdemo.wsdl.GetActivitiesForPatientResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.util.ArrayList;
import java.util.List;

public class ActivityClientService extends WebServiceGatewaySupport {

    public List<ActivityDTO> getActivities(int patientID) {
        GetActivitiesForPatientRequest request = new GetActivitiesForPatientRequest();
        request.setPatientId(patientID);

        GetActivitiesForPatientResponse response = (GetActivitiesForPatientResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://172.17.0.8:8080/ws/activities", request,
                        new SoapActionCallback(
                                "http://www.baeldung.com/springsoap/gen"
                        ));
        List<Activity> activities = response.getActivity();
        List<ActivityDTO> activityDTOS = new ArrayList<>();
        for(Activity a : activities) {
            ActivityDTO dto = new ActivityDTO();
            dto.setId(a.getId());
            dto.setActivityName(a.getActivityName());
            dto.setStartDate(a.getStartDate());
            dto.setEndDate(a.getEndDate());
            activityDTOS.add(dto);
        }
        return activityDTOS;
    }
}
