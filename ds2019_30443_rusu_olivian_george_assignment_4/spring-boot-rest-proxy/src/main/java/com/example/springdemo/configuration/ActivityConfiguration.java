package com.example.springdemo.configuration;

import com.example.springdemo.services.ActivityClientService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class ActivityConfiguration {


    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.example.springdemo.wsdl");
        return marshaller;
    }

    @Bean
    public ActivityClientService activityClient(Jaxb2Marshaller marshaller) {
        ActivityClientService client = new ActivityClientService();
        client.setDefaultUri("http://172.17.0.8:8080/ws");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
