package com.example.springdemo.endpoints;

import com.baeldung.springsoap.gen.GetActivitiesForPatientRequest;
import com.baeldung.springsoap.gen.GetActivitiesForPatientResponse;
import com.baeldung.springsoap.gen.ObjectFactory;
import com.example.springdemo.entities.Activity;
import com.example.springdemo.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class ActivityEndpoint {

    private static final String NAMESPACE_URI = "http://www.baeldung.com/springsoap/gen";
    private ActivityRepository activityRepository;

    @Autowired
    public ActivityEndpoint(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetActivitiesForPatientRequest")
    @ResponsePayload
    public GetActivitiesForPatientResponse getActivities(@RequestPayload GetActivitiesForPatientRequest request) {
        GetActivitiesForPatientResponse response1 = new ObjectFactory().createGetActivitiesForPatientResponse();
        List<Activity> activityList = activityRepository.findActivitiesByPatientId(request.getPatientId());
        List<com.baeldung.springsoap.gen.Activity> newActivities = new ArrayList<>();
        for(Activity a : activityList) {
            com.baeldung.springsoap.gen.Activity newActivity = new com.baeldung.springsoap.gen.Activity();
            newActivity.setActivityName(a.getActivityName());
            newActivity.setEndDate(a.getEndDate());
            newActivity.setStartDate(a.getStartDate());
            newActivity.setId(a.getId());
            newActivities.add(newActivity);
        }
        response1.getActivity().addAll(newActivities);
        return response1;
    }
}
