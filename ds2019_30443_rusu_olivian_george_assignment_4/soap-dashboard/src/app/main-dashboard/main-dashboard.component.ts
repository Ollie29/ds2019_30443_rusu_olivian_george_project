import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { ActivityInterface } from '../models/interfaces/activity';
import { ActivityModel } from '../models/activity.model';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.css']
})
export class MainDashboardComponent implements OnInit {

  patientID: string;
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [['Sleeping'], ['Toileting'], ['Breakfast'], ['Grooming'], 
                                  ['Spare_Time/TV'], ['Leaving'], ['Lunch'], ['Snack'], 'Showering'];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors: Array<any> = [ { backgroundColor: ['#feb195', '#7b9c0a', '#713b84', '#52a5d0', '#0e22f1', 
                              '#92133c', '#7f8238', '#41cffb', '#5298a2'], borderColor: 'transparent' } ];

  constructor(private apiService: ApiService, private activityModel: ActivityModel) {
      monkeyPatchChartJsTooltip();
      monkeyPatchChartJsLegend();
   }

  ngOnInit() {
    this.activityModel.all = [];
  }

  getActivities(ID: string) {
      this.apiService.getActivities(ID).subscribe(
        (data: Array<ActivityInterface>) =>
        {
          this.activityModel.all = data;
        }
      );
  }

  onEnterID(here: any) {
    console.log(here);
    if(here != undefined) {
      this.getActivities(here);
      console.log(this.activityModel.all);
      let res = this.activityModel.all.reduce(function(obj, v) {
      obj[v.activityName] = (obj[v.activityName] || 0) + 1;
      return obj;
    }, {});

      //console.log(res);
      this.pieChartData = [res['Sleeping'], res['Toileting'], res['Breakfast'], res['Grooming'], res['Spare_Time/TV'], 
                          res['Leaving'], res['Lunch'], res['Snack'], res['Showering']];
    }
  }



}
