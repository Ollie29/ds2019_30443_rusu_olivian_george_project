import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActivityInterface } from '../models/interfaces/activity';


@Injectable({
    providedIn: 'root'
})
export class ApiService {
    public BASE_URL: string = 'http://localhost:32824';

    constructor(private http: HttpClient) {}
    
    public getActivities(patientID: string):Observable<Array<ActivityInterface>> {
        const url = `${this.BASE_URL}/soap/get/activities/${patientID}`;
        return this.http.get<Array<ActivityInterface>>(url);
    }
}