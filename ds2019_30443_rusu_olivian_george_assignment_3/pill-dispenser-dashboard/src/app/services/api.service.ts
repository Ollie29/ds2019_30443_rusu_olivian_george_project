import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { MedicationPlanInterface } from '../models/interfaces/medicationplan';
import { Injectable } from '@angular/core';


@Injectable({
    providedIn: 'root'
})
export class ApiService {
    public BASE_URL: string = 'http://localhost:32822';

    constructor(private http: HttpClient) {}
    
    public getMedPlans():Observable<Array<MedicationPlanInterface>> {
        const url = `${this.BASE_URL}/client/medication/plan/get`;
        return this.http.get<Array<MedicationPlanInterface>>(url);
    }
}