import { MedicationPlanInterface } from './interfaces/medicationplan';
export class MedicationPlanModel {
    all: Array<MedicationPlanInterface>;
    selectedMedicationPlanId: string;

    setSelected(id: string) {
        return this.all.find((medicationPlan: MedicationPlanInterface) => medicationPlan.id === id);
    }

    removeSelected() {
        this.selectedMedicationPlanId = undefined;
    }

    clear() {
        this.removeSelected();
        this.all = undefined;
    }
}

