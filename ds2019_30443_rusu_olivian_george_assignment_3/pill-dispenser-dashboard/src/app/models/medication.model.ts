import { MedicationInterface } from './interfaces/medication';

export class MedicationModel {
    all: Array<MedicationInterface>;
}