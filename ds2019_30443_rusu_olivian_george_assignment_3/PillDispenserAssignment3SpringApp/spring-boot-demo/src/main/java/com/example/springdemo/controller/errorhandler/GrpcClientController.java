package com.example.springdemo.controller.errorhandler;


import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.services.GrpcClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/client/medication/plan")
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class GrpcClientController {

    @Autowired
    private GrpcClientService service;

    @GetMapping(value = "/get")
    public List<MedicationPlanDTO> getMedPlans() {
        return this.service.receiveMedicationPlan();
    }
}
