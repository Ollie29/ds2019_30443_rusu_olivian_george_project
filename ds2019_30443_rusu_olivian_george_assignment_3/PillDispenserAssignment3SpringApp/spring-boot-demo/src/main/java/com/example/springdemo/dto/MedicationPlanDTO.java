package com.example.springdemo.dto;

import java.util.List;

public class MedicationPlanDTO {

    private Integer id;
    private String startDate;
    private String endDate;
    private Integer intakeInterval;
    private List<MedicationDTO> medication;

    public MedicationPlanDTO() {
    }

    public MedicationPlanDTO(Integer id, String startDate, String endDate, Integer intakeInterval, List<MedicationDTO> medication) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.intakeInterval = intakeInterval;
        this.medication = medication;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(Integer intakeInterval) {
        this.intakeInterval = intakeInterval;
    }

    public List<MedicationDTO> getMedication() {
        return medication;
    }

    public void setMedication(List<MedicationDTO> medication) {
        this.medication = medication;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
