package com.example.springdemo.dto;

public class MedicationDTO {

    private String name;
    private Integer dosage;

    public MedicationDTO() {
    }

    public MedicationDTO(String name, Integer dosage) {
        this.name = name;
        this.dosage = dosage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
