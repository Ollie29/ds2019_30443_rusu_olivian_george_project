package com.example.springdemo.services.QueueReceiver;

import com.example.springdemo.Activity;

import com.example.springdemo.entities.ActivityEntity;
import com.example.springdemo.services.ActivityService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
public class QueueReceiver {

    private ActivityService activityService;
    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    public QueueReceiver(ActivityService activityService) {
        this.activityService = activityService;
    }

    /*
    public void receiveData() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C\n");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            byte[] byteArray = delivery.getBody();

            try {
                Activity activityReceived = (Activity) deserialize(byteArray);
                String startDate = resolveDate(activityReceived.getStartDate());
                String endDate = resolveDate(activityReceived.getEndDate());
                ActivityEntity activityToInsert = new ActivityEntity(activityReceived.getActivity(),
                        startDate, endDate);
                this.activityService.insertActivity(activityToInsert, activityReceived.getPatientId());
                System.out.println(" [x] Received new message: " + activityToInsert.toString() + "\n");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {

        });
    }*/

    @RabbitListener(queues = "activity_queue")
    public void receiveMessage(byte[] array) throws IOException, ClassNotFoundException {
        Activity activityReceived = (Activity) deserialize(array);
        String startDate = resolveDate(activityReceived.getStartDate());
        String endDate = resolveDate(activityReceived.getEndDate());
        String message;
        ActivityEntity activityToInsert = new ActivityEntity(activityReceived.getActivity(),
                startDate, endDate);
        this.activityService.insertActivity(activityToInsert, Integer.parseInt(activityReceived.getPatientId()));
        message = this.activityService.resolveAll(activityToInsert, Integer.parseInt(activityReceived.getPatientId()));
        System.out.println("Received new message!; " + activityReceived.toString());
        if(message != null) {
            System.out.println("MESSAGE SENT !!!");
            template.convertAndSend("/topic", message);
        }
    }

    private static String resolveDate(long date1) {
        Date date = new Date(date1);
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    private static Object deserialize(byte[] byteArray) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(byteArray);
        ObjectInputStream is = new ObjectInputStream(in);
        return is.readObject();
    }
}
