package com.example.springdemo.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "intakeInterval")
public class IntakeInterval {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "intakeInterval", fetch = FetchType.LAZY)
    private List<Medication> medicationList;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "end_date")
    private String endDate;

    @ManyToOne
    @JoinColumn
    private MedicationPlan medicationPlan;

    public IntakeInterval() {
    }

    public IntakeInterval(List<Medication> medicationList, String startDate, String endDate) {
        this.medicationList = medicationList;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public IntakeInterval(Integer id, String startDate, String endDate) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.medicationList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    @Override
    public String toString() {
        return "IntakeInterval{" +
                "id=" + id +
                ", medicationList=" + medicationList +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", medicationPlan=" + medicationPlan +
                '}';
    }
}
