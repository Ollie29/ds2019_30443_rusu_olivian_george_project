package com.example.springdemo.dto;

import com.example.springdemo.entities.MedicationPlan;

import java.util.List;

public class PatientWithPlansDTO {

    private Integer id;
    private String firstName;
    private String lastName;
    private List<MedicationPlan> medicationPlanList;

    public PatientWithPlansDTO() {
    }

    public PatientWithPlansDTO(Integer id, String firstName, String lastName, List<MedicationPlan> medicationPlanList) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.medicationPlanList = medicationPlanList;
    }

    public PatientWithPlansDTO(int id, String firstName, String lastName, List<MedicationPlanDTO> dtos) {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<MedicationPlan> getMedicationPlanList() {
        return medicationPlanList;
    }

    public void setMedicationPlanList(List<MedicationPlan> medicationPlanList) {
        this.medicationPlanList = medicationPlanList;
    }
}
