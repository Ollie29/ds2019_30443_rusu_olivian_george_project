package com.example.springdemo.services;

import com.example.springdemo.Activity;
import com.example.springdemo.MessageToSend;
import com.example.springdemo.entities.ActivityEntity;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.ActivityRepository;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
public class ActivityService {

    private ActivityRepository activityRepository;
    private PatientRepository patientRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository, PatientRepository patientRepository) {
        this.activityRepository = activityRepository;
        this.patientRepository = patientRepository;
    }

    public Integer insertActivity(ActivityEntity activityEntity, Integer patientID) {
        Optional<Patient> patient = this.patientRepository.findById(patientID);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "patient id", patientID);
        }
        activityEntity.setPatient(patient.get());
        //patient.get().getActivities().add(activityEntity);

        return this.activityRepository.save(activityEntity).getId();
    }

//    public MessageToSend resolveR1andR2(ActivityEntity activity, int patientID) {
//        String toSend = null;
//        if((activity.getActivityName().equals("Sleeping")) || (activity.getActivityName().equals("Leaving"))) {
//            String startDate = activity.getStartDate();
//            String endDate = activity.getEndDate();
//            startDate = startDate.replace(" ", "T");
//            endDate = endDate.replace(" ", "T");
//
//            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//            LocalDateTime startDateTime = LocalDateTime.parse(startDate);
//            LocalDateTime endDateTime = LocalDateTime.parse(endDate);
//
//            long difference = ChronoUnit.HOURS.between(startDateTime, endDateTime);
//
//            if(difference > 12) {
//                if(activity.getActivityName().equals("Sleeping")) {
//                    toSend = "PATIENT WITH ID: " + patientID + " HAS SLEPT MORE THAN 12 HOURS!\n";
//                    System.out.println("PATIENT WITH ID: " + patientID + " HAS SLEPT MORE THAN 12 HOURS!\n");
//                }
//                else {
//                    toSend = "PATIENT WITH ID: " + patientID + " HAS LEFT FOR MORE THAN 12 HOURS!\n";
//                    System.out.println("PATIENT WITH ID: " + patientID + " HAS LEFT FOR MORE THAN 12 HOURS!\n");
//                }
//            }
//        }
//        return new MessageToSend(toSend);
//    }
//
//    public MessageToSend resolveR3(ActivityEntity activity, int patientID) {
//        String activityString = activity.getActivityName();
//        String toSend = null;
//        if(activityString.equals("Toileting") || activityString.equals("Showering")) {
//            String startDate = activity.getStartDate();
//            String endDate = activity.getEndDate();
//            startDate = startDate.replace(" ", "T");
//            endDate = endDate.replace(" ", "T");
//
//            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//            LocalDateTime startDateTime = LocalDateTime.parse(startDate);
//            LocalDateTime endDateTime = LocalDateTime.parse(endDate);
//
//            long difference = ChronoUnit.HOURS.between(startDateTime, endDateTime);
//            if(difference > 1) {
//                toSend = "PATIENT WITH ID: " + patientID + "HAS SPENT MORE THAN 1 HOUR IN THE BATHROOM!\n";
//                System.out.println("PATIENT WITH ID: " + patientID + "HAS SPENT MORE THAN 1 HOUR IN THE BATHROOM!\n");
//            }
//        }
//        return new MessageToSend(toSend);
//    }

    public String resolveAll(ActivityEntity activity, int patientID) {

        String toSend = null;
        String startDate = activity.getStartDate();
        String endDate = activity.getEndDate();
        startDate = startDate.replace(" ", "T");
        endDate = endDate.replace(" ", "T");

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startDateTime = LocalDateTime.parse(startDate);
        LocalDateTime endDateTime = LocalDateTime.parse(endDate);

        long difference = ChronoUnit.HOURS.between(startDateTime, endDateTime);

        if ((activity.getActivityName().equals("Sleeping")) || (activity.getActivityName().equals("Leaving"))) {
            if (difference > 12) {
                if (activity.getActivityName().equals("Sleeping")) {
                    toSend = "PATIENT WITH ID: " + patientID + " HAS SLEPT MORE THAN 12 HOURS!\n";
                    System.out.println("PATIENT WITH ID: " + patientID + " HAS SLEPT MORE THAN 12 HOURS!\n");
                } else {
                    toSend = "PATIENT WITH ID: " + patientID + " HAS LEFT FOR MORE THAN 12 HOURS!\n";
                    System.out.println("PATIENT WITH ID: " + patientID + " HAS LEFT FOR MORE THAN 12 HOURS!\n");
                }
            }
        } else {
            if ((activity.getActivityName().equals("Toileting")) || (activity.getActivityName().equals("Showering"))) {
                if (difference > 1) {
                    toSend = "PATIENT WITH ID: " + patientID + "HAS SPENT MORE THAN 1 HOUR IN THE BATHROOM!\n";
                    System.out.println("PATIENT WITH ID: " + patientID + "HAS SPENT MORE THAN 1 HOUR IN THE BATHROOM!\n");
                }
            }
        }
        return toSend;
    }

}

