package com.example.springdemo.controller;

import com.example.springdemo.dto.IntakeIntervalDTO;
import com.example.springdemo.entities.IntakeInterval;
import com.example.springdemo.services.IntakeIntervalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/intake")
public class IntakeIntervalController {

    private final IntakeIntervalService service;

    @Autowired
    public IntakeIntervalController(IntakeIntervalService intake) {
        this.service = intake;
    }

    @PutMapping(value = "/add/medicine/{id}")
    public IntakeIntervalDTO addMedicineToIntakeInterval(@PathVariable("id") Integer id, @RequestBody IntakeIntervalDTO intakeIntervalDTO) {
        IntakeInterval intakeInterval = service.insertIntakeIntervalForMedication(intakeIntervalDTO, id);
        return new IntakeIntervalDTO(intakeInterval.getId(), intakeInterval.getStartDate(), intakeInterval.getEndDate());
    }

    @DeleteMapping(value = "delete/{id}")
    public void deleteIntakeInterval(@PathVariable("id") Integer id) {
        service.deleteIntakeInterval(id);
    }
}
