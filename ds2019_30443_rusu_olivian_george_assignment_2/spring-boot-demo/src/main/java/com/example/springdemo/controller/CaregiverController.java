package com.example.springdemo.controller;

import com.example.springdemo.Activity;
import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;
    //private final static String QUEUE_NAME = "activity_queue";

    @Autowired
    public CaregiverController(CaregiverService service) {
        this.caregiverService = service;
    }

    @GetMapping(value = "/id/{id}")
    public CaregiverDTO getById(@PathVariable("id") Integer id) {
        return caregiverService.findCaregiverById(id);
    }

    @GetMapping
    public List<CaregiverDTO> getAllCaregivers() {
        return caregiverService.findAll();
    }

    @PostMapping(value = "/insert")
    public Integer insertCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.insertCaregiver(caregiverDTO);
    }

    @PutMapping(value = "/update")
    public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.updateCaregiver(caregiverDTO);
    }

    @DeleteMapping(value = "delete/{id}")
    public void deleteCaregiver(@PathVariable("id") Integer id) {
        caregiverService.deleteCaregiver(id);
    }

    @PutMapping(value = "/add/patient/{id}")
    public void addPatientToCaregiver(@PathVariable("id") Integer id, @RequestBody PatientDTO patientDTO) {
        caregiverService.addPatientToCaregiver(id, patientDTO);
    }

    @GetMapping(value = "patients/{id}")
    public List<PatientViewDTO> getAllPatientsFromCaregiver(@PathVariable("id") Integer id) {
        return caregiverService.viewAllPatientsFromCaregiver(id);
    }

//    @GetMapping(value = "/monitor/patients")
//    public void monitorPatients() throws IOException, TimeoutException {
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
//        Connection connection = factory.newConnection();
//        Channel channel = connection.createChannel();
//        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
//        System.out.println(" [*] Waiting for messages. To exit press CTRL+C\n");
//
//        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
//            byte[] byteArray = delivery.getBody();
//
//            try {
//                Activity activityReceived = (Activity) this.deserialize(byteArray);
//                System.out.println(" [x] Received new message: " + activityReceived.toString() + "\n");
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
//        };
//        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
//
//        });
//    }
//
//    private Object deserialize(byte[] byteArray) throws IOException, ClassNotFoundException {
//        ByteArrayInputStream in = new ByteArrayInputStream(byteArray);
//        ObjectInputStream is = new ObjectInputStream(in);
//        return is.readObject();
//    }
}
