package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeIntervalDTO;
import com.example.springdemo.entities.IntakeInterval;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.IntakeIntervalRepository;
import com.example.springdemo.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;


@Service
public class IntakeIntervalService {

    private IntakeIntervalRepository intakeIntervalRepository;
    private MedicationRepository medicationRepository;

    @Autowired
    public IntakeIntervalService(IntakeIntervalRepository repo, MedicationRepository medicationRepository) {
        this.intakeIntervalRepository = repo;
        this.medicationRepository = medicationRepository;
    }

    public IntakeInterval insertIntakeIntervalForMedication(IntakeIntervalDTO intakeIntervalDTO, Integer medID) {

        Optional<Medication> medication = medicationRepository.findById(medID);
        if(!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "medication id", medID);
        }
        IntakeInterval intakeInterval = new IntakeInterval(intakeIntervalDTO.getId(), intakeIntervalDTO.getStartDate(), intakeIntervalDTO.getEndDate());
        Medication medicationToInsert = medication.get();
        intakeInterval.getMedicationList().add(medicationToInsert);
        medicationToInsert.setIntakeInterval(intakeInterval);
        intakeIntervalRepository.save(intakeInterval).getId();

        return intakeInterval;
    }

    public void deleteIntakeInterval(Integer id) {
        intakeIntervalRepository.deleteById(id);
    }


}