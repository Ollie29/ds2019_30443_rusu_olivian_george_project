package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverViewDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverViewBuilder {

    public CaregiverViewBuilder() {
    }

    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverViewDTO(
                caregiver.getFirstName(),
                caregiver.getLastName(),
                caregiver.getBirthDate(),
                caregiver.getGenderType(),
                caregiver.getAddress()
        );
    }

    public static Caregiver generateEntityFromDro(PatientViewDTO dto) {
        return new Caregiver(
                dto.getFirstName(),
                dto.getLastName(),
                dto.getBirthDate(),
                dto.getGender(),
                dto.getAddress()
        );
    }
}
