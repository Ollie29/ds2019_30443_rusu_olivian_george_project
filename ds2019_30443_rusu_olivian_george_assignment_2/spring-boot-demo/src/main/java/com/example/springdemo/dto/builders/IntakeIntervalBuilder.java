package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.IntakeIntervalDTO;
import com.example.springdemo.entities.IntakeInterval;

public class IntakeIntervalBuilder {

    public IntakeIntervalBuilder() {
    }

    public static IntakeIntervalDTO generateDTOFromEntity(IntakeInterval intakeInterval) {
        return new IntakeIntervalDTO(
                intakeInterval.getId(),
                intakeInterval.getStartDate(),
                intakeInterval.getEndDate()
        );
    }

    public static IntakeInterval generateEntityFromDTO(IntakeIntervalDTO intakeIntervalDTO) {
        return new IntakeInterval(
                intakeIntervalDTO.getId(),
                intakeIntervalDTO.getStartDate(),
                intakeIntervalDTO.getEndDate()
        );
    }
}
