import com.example.springdemo.Activity;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.concurrent.TimeUnit;

class Processing {

    static void processData() throws Exception {
//        try {
//            Stream<String> stream = Files.lines(Paths.get("activity.txt"));
//            this.listOfActivities = stream
//                                    .map(com.example.springdemo.Activity::new)
//                                    .collect(toCollection(ArrayList::new));
//        }
//        catch(IOException ex) {
//            System.out.println("IOException in file\n");
//        }
//
        try (BufferedReader br = new BufferedReader(new FileReader("activity.txt"))) {
            String line;
            while((line = br.readLine()) != null) {
                Activity a = new Activity(line);
                Producer.send(a);
                TimeUnit.SECONDS.sleep(1);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
