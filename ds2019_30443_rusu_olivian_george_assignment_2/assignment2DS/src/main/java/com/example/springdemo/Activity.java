package com.example.springdemo;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Activity implements Serializable {

    private static final long serialVersionUID = 6529685098267757690L;
    private String patientId;
    private String activity;
    private long startDate;
    private long endDate;

    public Activity(String s) {

        String[] data = s.split("[\\t]+");
        try {
            if(data.length != 3) {
                throw new ParseException("String not in the right format!", 0);
            }
            this.startDate = convertDateToEpoch(data[0]);
            this.endDate = convertDateToEpoch(data[1]);
        } catch(ParseException ex) {
            System.out.println("Parse exception when creating new activity object!\n");
        }
        this.activity = data[2].trim();

        double randomDouble = Math.random(); // generating a random patient id
        randomDouble = randomDouble * 30 + 1;
        this.patientId = "19";//String.valueOf((int) randomDouble);
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    private long convertDateToEpoch(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.parse(date).getTime();
    }

    @Override
    public String toString() {
        return "com.example.springdemo.Activity{" +
                "patientId='" + patientId + '\'' +
                ", activity='" + activity + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
