import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Producer {

    private final static String QUEUE_NAME = "activity_queue";

    public static void send(Object obj) throws Exception{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost"); //connection to a broker on the local machine (name or IP otherwise)
        factory.setPort(32769);
        try(Connection connection = factory.newConnection(); //try-with-resources
            Channel channel = connection.createChannel()) {

            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            byte[] byteArray = getByteArray(obj);

            channel.basicPublish("", QUEUE_NAME, false,null, byteArray);
            System.out.println(" [x] SENT ' " + obj + " '");

        }
    }

    private static byte[] getByteArray(Object obj) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(obj);
        return byteArrayOutputStream.toByteArray();
    }
}
