import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
    
    constructor(private storageService: StorageService, private router: Router) {}

    /**
     * Adds the token to all outgoing requests
     * @param req 
     * @param next 
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let authCode: string = this.storageService.get(this.storageService.appToken);

        let modifiedReq = req.clone({
            setHeaders: { Authorization: `Basic ${authCode}` }
        });

        return next.handle(modifiedReq);
    }


    
}