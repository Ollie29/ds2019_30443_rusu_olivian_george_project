import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
    public appKey = 'medication_platform';
    public appToken = `${this.appKey}_token`;


    /**
     * Get app key from local storage
     * @param key 
     */
    public get(key: string): string {
        return localStorage.getItem(key);
    }


    /**
     * Set a key in local storage
     * @param key 
     * @param value 
     */
    public set(key: string, value: string): string {
        localStorage.setItem(key, value);
        return this.get(key);
    }

    /**
     * Remove a key from local storage
     * @param key 
     */
    public remove(key: string): boolean {
        localStorage.removeItem(key);
        return this.get(key) ? false : true;
    }
}