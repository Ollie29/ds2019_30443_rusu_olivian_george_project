import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PatientInterface } from '../models/interfaces/patient';
import { CaregiverInterface } from '../models/interfaces/caregiver';
import { MedicationInterface } from '../models/interfaces/medication';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    public BASE_URL: string = 'http://localhost:32783';
    
    constructor(private http: HttpClient) {}

    public getPatients(): Observable<Array<PatientInterface>> {
        const url = `${this.BASE_URL}/patient`;
        return this.http.get<Array<PatientInterface>>(url);
    }

    public getPatientById(id: string): Observable<PatientInterface> {
        const url = `${this.BASE_URL}/patient/id/${id}`;
        return this.http.get<PatientInterface>(url);
    }

    public savePatientData(patientData: PatientInterface, caregiverID: string): Observable<PatientInterface> {
        const url = `${this.BASE_URL}/patient/update/${caregiverID}`;
        return this.http.put<PatientInterface>(url, patientData);
    }

    public deletePatient(patientId: string): Observable<any> {
        const url = `${this.BASE_URL}/patient/delete/${patientId}`;
        return this.http.delete<any>(url);
    }

    public insertPatient(patientData: PatientInterface, caregiverID: string): Observable<PatientInterface> {
        const url = `${this.BASE_URL}/patient/insert/${caregiverID}`;
        return this.http.post<any>(url, patientData);
    }

    public getCaregivers(): Observable<Array<CaregiverInterface>> {
        const url = `${this.BASE_URL}/caregiver`;
        return this.http.get<Array<CaregiverInterface>>(url);
    }

    public getCaregiverById(id: string): Observable<CaregiverInterface> {
        const url = `${this.BASE_URL}/caregiver/id/${id}`;
        return this.http.get<CaregiverInterface>(url);
    }

    public deleteCaregiver(id: string): Observable<any> {
        const url = `${this.BASE_URL}/caregiver/delete/${id}`;
        return this.http.delete<any>(url);
    }

    public saveCaregiverData(caregiverData: CaregiverInterface): Observable<CaregiverInterface> {
        const url = `${this.BASE_URL}/caregiver/update`;
        return this.http.put<CaregiverInterface>(url, caregiverData);
    }

    public insertCaregiver(caregiverData: CaregiverInterface): Observable<CaregiverInterface> {
        const url = `${this.BASE_URL}/caregiver/insert/`;
        return this.http.post<any>(url, caregiverData);
    }

    public getMedication(): Observable<Array<MedicationInterface>> {
        const url = `${this.BASE_URL}/medication`;
        return this.http.get<Array<MedicationInterface>>(url);
    }

    public getMedicationById(id: string): Observable<MedicationInterface> {
        const url = `${this.BASE_URL}/medication/id/${id}`;
        return this.http.get<MedicationInterface>(url);
    }

    public insertMedication(medicationData: MedicationInterface): Observable<MedicationInterface> {
        const url = `${this.BASE_URL}/medication/insert/`;
        return this.http.post<any>(url, medicationData);
    }

    public saveMedicationData(medicationData: MedicationInterface): Observable<MedicationInterface> {
        const url = `${this.BASE_URL}/medication/update`;
        return this.http.put<MedicationInterface>(url, medicationData);
    }

    public deleteMedication(id: string): Observable<any> {
        const url = `${this.BASE_URL}/medication/delete/${id}`;
        return this.http.delete<MedicationInterface>(url);
    }

    // public getCaregiverIdByUserId(username: string): Observable<any> {
    //     const url = `${this.BASE_URL}/caregiver/find/${username}`;
    //     return this.http.get<any>(url);
    // }

    public getPatientsInCare(username: string): Observable<Array<PatientInterface>> {
        const url = `${this.BASE_URL}/caregiver/patients-in-care/${username}`;
        return this.http.get<Array<PatientInterface>>(url);
    }
}
