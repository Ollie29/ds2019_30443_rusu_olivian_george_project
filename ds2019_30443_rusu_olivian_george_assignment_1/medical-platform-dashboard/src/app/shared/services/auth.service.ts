import { HttpClient, HttpHeaders } from "@angular/common/http";
import { StorageService } from "./storage.service";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class AuthService {

    public BASE_URL: string = 'http://localhost:32783';

    constructor(private http: HttpClient, private storageService: StorageService) {}

    /**
     * Sends credentials to the backend
     * @param username 
     * @param password 
     * @param authCode 
     */
    login(username: string, password: string, authCode: string): Observable<any> {
        let url = `${this.BASE_URL}/application/login`;
        let header: any = new HttpHeaders({
            Authorization: `Basic ${authCode}`
        });
        sessionStorage.setItem('loggedUser', username);
        return this.http.get(url, {headers: header});
    }
    
    /**
     * Checks if the application token is already stored in the browser
     */
    isLoggedIn(): boolean {
        let token: any = this.storageService.get(this.storageService.appToken);
        return token ? true : false;
    }


}