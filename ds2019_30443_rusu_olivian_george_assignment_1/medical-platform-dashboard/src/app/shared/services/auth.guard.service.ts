import { AuthService } from './auth.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {}


    /**
     * If authed, can continue to next child route
     * @param route 
     * @param state 
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : any {
        let isAuthed: boolean = this.authService.isLoggedIn();

        if(isAuthed) {
            return isAuthed;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}