import {PatientInterface} from "./patient";
export interface CaregiverInterface {

    id: string;

    firstName: string;

    lastName: string;

    birthDate: string;

    genderType: string;

    address: string;

    patientList: Array<PatientInterface>;
}