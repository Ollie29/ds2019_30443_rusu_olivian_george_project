import { CaregiverInterface } from './caregiver';

export interface PatientInterface {
    
    id: string;

    firstName: string;

    lastName: string;

    birthDate: string;

    gender: string;

    address: string;

    caregiverDTO: CaregiverInterface;
}