export class MedicationInterface {
    id: string;
    
    name: string;

    sideEffects: string;

    dosage: string;
}