import { Injectable } from '@angular/core';
import { MedicationInterface } from './interfaces/medication';

@Injectable()
export class MedicationModel {

    all: Array<MedicationInterface>;
    selectedMedicationId: string;

    setSelected(id: string) {
        return this.all.find((medication: MedicationInterface) => medication.id === id);
    }

    removeSelected() {
        this.selectedMedicationId = undefined;
    }

    clear() {
        this.removeSelected();
        this.all = undefined;
    }

}