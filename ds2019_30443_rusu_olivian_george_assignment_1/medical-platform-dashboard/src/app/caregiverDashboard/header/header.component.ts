import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-caregiver-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class CaregiverDashboardHeaderComponent implements OnInit {

  constructor(private storageService: StorageService, private route: Router) { }

  ngOnInit() {
  }

  logOut() {
    this.storageService.remove(this.storageService.appToken);
    this.route.navigate(['/login']);
  }
}
