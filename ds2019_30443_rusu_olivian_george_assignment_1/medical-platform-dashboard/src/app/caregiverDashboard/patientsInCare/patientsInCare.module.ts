import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ApiService } from 'src/app/shared/services/api.service';
import { NgModule } from '@angular/core';
import { PatientsInCareRoutingModule } from './PatientsInCare-routing.module';
import { PatientsInCareListComponent } from './patients-in-care-list/patients-in-care-list.component';

@NgModule({
    declarations: [PatientsInCareListComponent],
  imports: [
    CommonModule,
    PatientsInCareRoutingModule,
    FormsModule
  ],
  providers: [
    ApiService
  ]
})
export class PatientsInCareModule {}