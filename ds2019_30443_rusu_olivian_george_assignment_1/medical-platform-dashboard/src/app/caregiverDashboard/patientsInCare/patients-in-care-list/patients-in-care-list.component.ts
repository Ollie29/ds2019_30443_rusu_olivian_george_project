import { Component, OnInit } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/shared/services/api.service';
import { PatientModel } from 'src/app/shared/models/patient.model';
import { PatientInterface } from 'src/app/shared/models/interfaces/patient';

@Component({
  selector: 'app-patients-in-care-list',
  templateUrl: './patients-in-care-list.component.html',
  styleUrls: ['./patients-in-care-list.component.css']
})
export class PatientsInCareListComponent implements OnInit {

  username: string = sessionStorage.getItem('loggedUser');
  private caregiverId: string;
  messages: any[] = [];
  webSocketEndPoint: string = "http://localhost:32783/notification/socket";
  private stompClient: any;
 
  constructor(private toastr: ToastrService, private apiService: ApiService, public patientModel: PatientModel) { }

  ngOnInit() {
    this.connect();
    this.getPatientsInCare();
  }

  connect() {
    console.log("Init WebSocket Connection\n");
    let ws = new SockJS(this.webSocketEndPoint);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
        that.stompClient.subscribe("/topic", function(message){
            console.log(message.body);
            that.messages.push(message.body);
            that.toastr.success(message.body);
        })
    });
  }

  getPatientsInCare() {
    this.apiService.getPatientsInCare(this.username).subscribe(
      (data: Array<PatientInterface>) => {
        this.patientModel.all = data;
      }
    );
  }
  
}
