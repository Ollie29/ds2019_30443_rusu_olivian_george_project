import { AuthGuard } from '../shared/services/auth.guard.service';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CaregiverDashboardMainComponent } from './main/main.component';

const routes: Routes = [{
    path: '',
    component: CaregiverDashboardMainComponent,
    canActivate: [AuthGuard],
    children: [
        { path: '', redirectTo: 'patients', pathMatch: 'full' },
        { path: 'patients', loadChildren: './patientsInCare/patientsInCare.module#PatientsInCareModule' }
    ]
}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CaregiverDashboardRoutingModule {}