import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptorService } from '../shared/interceptor/http.interceptor';
import { AuthGuard } from '../shared/services/auth.guard.service';
import { AuthService } from '../shared/services/auth.service';
import { StorageService } from '../shared/services/storage.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaregiverDashboardMainComponent } from './main/main.component';
import { CaregiverDashboardHeaderComponent } from './header/header.component';
import { CaregiverDashboardRoutingModule } from './caregiver-dashboard-routing.module';

@NgModule({
    declarations: [CaregiverDashboardHeaderComponent, CaregiverDashboardMainComponent],
    imports: [CommonModule, HttpClientModule, CaregiverDashboardRoutingModule],
    providers: [
        AuthGuard,
        AuthService,
        StorageService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        }
    ]
})
export class CaregiverDashboardModule {}