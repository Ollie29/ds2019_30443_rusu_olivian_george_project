import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { MedicationInterface } from 'src/app/shared/models/interfaces/medication';

@Component({
  selector: 'app-medication-insert',
  templateUrl: './medication-insert.component.html',
  styleUrls: ['./medication-insert.component.css']
})
export class MedicationInsertComponent implements OnInit {

  medication: MedicationInterface = {} as MedicationInterface;

  constructor(private router: Router, private apiService: ApiService, private route: ActivatedRoute, private toastr: ToastrService) { }

  ngOnInit() {
  }

  onMedicationCreate() {
     this.medication.id = '0';
     this.apiService.insertMedication(this.medication).subscribe(
       () => {
         this.toastr.success("Medication inserted successfuly!");
       }
     );
  }

}
