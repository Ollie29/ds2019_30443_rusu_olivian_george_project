import { Component, OnInit } from '@angular/core';

import { ApiService } from 'src/app/shared/services/api.service';
import { MedicationInterface } from 'src/app/shared/models/interfaces/medication';
import { MedicationModel } from 'src/app/shared/models/medication.model';

@Component({
  selector: 'app-medication-list',
  templateUrl: './medication-list.component.html',
  styleUrls: ['./medication-list.component.css']
})
export class MedicationListComponent implements OnInit {

  constructor(public medicationModel: MedicationModel, private apiService: ApiService) { }

  ngOnInit() {
    this.getAllMedication();
  }

  getAllMedication() {
    this.apiService.getMedication().subscribe(
      (data: Array<MedicationInterface>) => {
        this.medicationModel.all = data;
      }
    );
  }
}
