import { Component, OnInit } from '@angular/core';
import { MedicationInterface } from 'src/app/shared/models/interfaces/medication';
import { MedicationModel } from 'src/app/shared/models/medication.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-medication-details',
  templateUrl: './medication-details.component.html',
  styleUrls: ['./medication-details.component.css']
})
export class MedicationDetailsComponent implements OnInit {

  medication: MedicationInterface = {} as MedicationInterface;
  private id: string;

  constructor(private router: Router, private apiService: ApiService, private route: ActivatedRoute, private medicationModel: MedicationModel, private toastr: ToastrService) { }

  ngOnInit() {

    this.route.params
      .subscribe(
          (params : Params) => {
            this.id = params['id'];
          }
      );
      this.getMedicationByID();
  }

  getMedicationByID(): void {
    this.apiService.getMedicationById(this.id).subscribe(
        (data: MedicationInterface) => {this.medication = data; } 
    );
}

onMedicationSave(): void {
    this.apiService.saveMedicationData(this.medication).subscribe(
      () => {
        this.toastr.success('Medication updated!');
      }
    );
}

onMedicationDelete(): void {
  this.apiService.deleteMedication(this.id).subscribe(
    () => {
      this.toastr.success('Medication removed!');
    }
  );
  this.router.navigate(['/medication']);
}

}
