import {Routes, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { PatientInsertComponent } from './patient-insert/patient-insert.component';

const routes: Routes = [
    { path: '', component: PatientListComponent },
    { path: ':id/edit', component: PatientDetailsComponent },
    { path: 'insert', component: PatientInsertComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PatientRoutingModule {}