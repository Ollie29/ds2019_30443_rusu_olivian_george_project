import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PatientModel } from 'src/app/shared/models/patient.model';
import { PatientInterface } from 'src/app/shared/models/interfaces/patient';
import { ToastrService } from 'ngx-toastr';
import { CaregiverInterface } from 'src/app/shared/models/interfaces/caregiver';
import { CaregiverModel } from 'src/app/shared/models/caregiver.model';

@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.css']
})
export class PatientDetailsComponent implements OnInit {

  patient: PatientInterface = {} as PatientInterface;
  private id: string;
  caregiverID: string = '';
  private caregiver: CaregiverInterface = {} as CaregiverInterface;

  constructor(private router: Router, private apiService: ApiService, private route: ActivatedRoute, public caregiverModel: CaregiverModel, public patientModel: PatientModel, private toastr: ToastrService) { }

  ngOnInit() {
    this.route.params
      .subscribe(
          (params : Params) => {
            this.id = params['id'];
          }
      );
    this.getPatientByID();
    this.getAllCaregivers();
  }

  getPatientByID(): void {
      this.apiService.getPatientById(this.id).subscribe(
          (data: PatientInterface) => {this.patient = data; 
          } 
      );
  }

  onPatientSave(): void {
      this.apiService.savePatientData(this.patient, this.caregiverID).subscribe(
        () => {
          this.toastr.success('Patient updated!');
        }
      );
  }

  getAllCaregivers() {
    this.apiService.getCaregivers().subscribe(
      (data: Array<CaregiverInterface>) => {
        this.caregiverModel.all = data;
      }
    );
  }

  onPatientDelete(): void {
    this.apiService.deletePatient(this.id).subscribe(
      () => {
        this.toastr.success('Patient removed!');
      }
    );
    this.router.navigate(['/patients']);
  }
  


}
