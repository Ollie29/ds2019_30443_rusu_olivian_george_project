import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientListComponent } from './patient-list/patient-list.component';
import { ApiService } from '../../shared/services/api.service';
import { PatientRoutingModule } from './patient-routing.module';
import { FormsModule } from '@angular/forms';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { PatientInsertComponent } from './patient-insert/patient-insert.component';


@NgModule({
  declarations: [PatientListComponent, PatientDetailsComponent, PatientInsertComponent],
  imports: [
    CommonModule,
    PatientRoutingModule,
    FormsModule
  ],
  providers: [
    ApiService
  ]
})
export class PatientModule { }
