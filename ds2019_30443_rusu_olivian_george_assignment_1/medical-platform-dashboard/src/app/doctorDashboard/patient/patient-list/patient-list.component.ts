import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { PatientModel } from 'src/app/shared/models/patient.model';
import { ApiService } from 'src/app/shared/services/api.service';
import { PatientInterface } from 'src/app/shared/models/interfaces/patient';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  private verify: boolean = true;

  constructor(public patientModel: PatientModel, private apiService: ApiService) { }

  ngOnInit() {
    this.getAllPatients();
    
  }

  getAllPatients() {
    this.apiService.getPatients().subscribe(
        //on Success save the patient in the patient model
        (data: Array<PatientInterface>) => 
          {this.patientModel.all = data;
            console.log(this.patientModel.all);
          }
       
        //on error, log it
    );
  }


}
