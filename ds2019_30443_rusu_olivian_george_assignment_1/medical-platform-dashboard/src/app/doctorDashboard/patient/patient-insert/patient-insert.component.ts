import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api.service';
import { PatientModel } from 'src/app/shared/models/patient.model';
import { CaregiverModel } from 'src/app/shared/models/caregiver.model';
import { ToastrService } from 'ngx-toastr';
import { PatientInterface } from 'src/app/shared/models/interfaces/patient';
import { CaregiverInterface } from 'src/app/shared/models/interfaces/caregiver';

@Component({
  selector: 'app-patient-insert',
  templateUrl: './patient-insert.component.html',
  styleUrls: ['./patient-insert.component.css']
})
export class PatientInsertComponent implements OnInit {

  patient: PatientInterface = {} as PatientInterface;
  caregiverID: string = '';


  constructor(private router: Router, private apiService: ApiService, private route: ActivatedRoute, public patientModel: PatientModel, private toastr: ToastrService, public caregiverModel: CaregiverModel) { }

  ngOnInit() {
    this.getAllCaregivers();
    console.log(this.caregiverModel.all);
  }

  onPatientCreate() {
    console.log(this.caregiverID);
     this.patient.id = '0';
     this.apiService.insertPatient(this.patient, this.caregiverID).subscribe(
       () => {
         this.toastr.success("Patient inserted successfuly!");
         this.caregiverID = '';
       }
     );
  }

  getAllCaregivers() {
    this.apiService.getCaregivers().subscribe(
      (data: Array<CaregiverInterface>) => {
        this.caregiverModel.all = data;
      }
    );
  }





}
