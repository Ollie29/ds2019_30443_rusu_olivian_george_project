import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage.service';

@Component({
  selector: 'app-doctor-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class DoctorDashboardHeaderComponent implements OnInit {

  constructor(private router: Router, private storageService: StorageService) { }

  ngOnInit() {
  }

  logOut() {
    this.storageService.remove(this.storageService.appToken);

    this.router.navigate(['/login']);
  }

}
