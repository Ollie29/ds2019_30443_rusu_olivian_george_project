import { NgModule } from "@angular/core";
import { RouterModule, Route, Routes } from "@angular/router";
import { AuthGuard } from "../shared/services/auth.guard.service";
import { DoctorDashboardMainComponent } from './main/main.component';

const routes: Routes = [{
    path: '',
    component: DoctorDashboardMainComponent,
    canActivate: [AuthGuard],
    children: [
        { path: '', redirectTo: 'patients', pathMatch: 'full' },
        { path: 'patients', loadChildren: './patient/patient.module#PatientModule' },
        { path: 'caregivers', loadChildren: './caregiver/caregiver.module#CaregiverModule' },
        { path: 'medication', loadChildren: './medication/medication.module#MedicationModule' }
    ]
}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DoctorDashboardRoutingModule {}