import { Component, OnInit } from '@angular/core';
import { CaregiverModel } from 'src/app/shared/models/caregiver.model';
import { ApiService } from 'src/app/shared/services/api.service';
import { CaregiverInterface } from 'src/app/shared/models/interfaces/caregiver';

@Component({
  selector: 'app-caregiver-list',
  templateUrl: './caregiver-list.component.html',
  styleUrls: ['./caregiver-list.component.css']
})
export class CaregiverListComponent implements OnInit {

  constructor(public caregiverModel: CaregiverModel, private apiService: ApiService) { }

  ngOnInit() {
    this.getAllCaregivers();
  }

  getAllCaregivers() {
    this.apiService.getCaregivers().subscribe(
      (data: Array<CaregiverInterface>) => {
        this.caregiverModel.all = data;
      }
    );
  }
}
