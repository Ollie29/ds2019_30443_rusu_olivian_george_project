import {Routes, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { CaregiverListComponent } from './caregiver-list/caregiver-list.component';
import { CaregiverDetailsComponent } from './caregiver-details/caregiver-details.component';
import { CaregiverInsertComponent } from './caregiver-insert/caregiver-insert.component';

const routes: Routes = [
    { path: '', component: CaregiverListComponent },
    { path: ':id/edit', component: CaregiverDetailsComponent },
    { path: 'insert', component: CaregiverInsertComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CaregiverRoutingModule {}