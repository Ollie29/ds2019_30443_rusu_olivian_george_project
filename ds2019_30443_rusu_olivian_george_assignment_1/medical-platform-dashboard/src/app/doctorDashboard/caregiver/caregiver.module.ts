import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../../shared/services/api.service';
import { FormsModule } from '@angular/forms';
import { CaregiverListComponent } from './caregiver-list/caregiver-list.component';
import { CaregiverRoutingModule } from './caregiver-routing.module';
import { CaregiverDetailsComponent } from './caregiver-details/caregiver-details.component';
import { CaregiverInsertComponent } from './caregiver-insert/caregiver-insert.component';


@NgModule({
  declarations: [CaregiverListComponent, CaregiverDetailsComponent, CaregiverInsertComponent],
  imports: [
    CommonModule,
    CaregiverRoutingModule,
    FormsModule
  ],
  providers: [
    ApiService
  ]
})
export class CaregiverModule { }
