import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'doctor', loadChildren: './doctorDashboard/doctor-dashboard.module#DoctorDashboardModule' }, //lazyLoading
 // { path: 'patient', loadChildren: '/' },
  { path: 'caregiver', loadChildren: './caregiverDashboard/caregiver-dashboard.module#CaregiverDashboardModule' },
  { path: 'login', loadChildren: './auth/auth.module#AuthModule' },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
