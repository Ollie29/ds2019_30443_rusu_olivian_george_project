package com.example.springdemo.controller;

import com.example.springdemo.Activity;
import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService service) {
        this.caregiverService = service;
    }

    @GetMapping(value = "/id/{id}")
    public CaregiverDTO getById(@PathVariable("id") Integer id) {
        return caregiverService.findCaregiverById(id);
    }

    @GetMapping
    public List<CaregiverDTO> getAllCaregivers() {
        return caregiverService.findAll();
    }

    @PostMapping(value = "/insert")
    public Integer insertCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.insertCaregiver(caregiverDTO);
    }

    @PutMapping(value = "/update")
    public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.updateCaregiver(caregiverDTO);
    }

    @DeleteMapping(value = "delete/{id}")
    public void deleteCaregiver(@PathVariable("id") Integer id) {
        caregiverService.deleteCaregiver(id);
    }

    @PutMapping(value = "/add/patient/{id}")
    public void addPatientToCaregiver(@PathVariable("id") Integer id, @RequestBody PatientDTO patientDTO) {
        caregiverService.addPatientToCaregiver(id, patientDTO);
    }

    @GetMapping(value = "patients/{id}")
    public List<PatientViewDTO> getAllPatientsFromCaregiver(@PathVariable("id") Integer id) {
        return caregiverService.viewAllPatientsFromCaregiver(id);
    }

    @GetMapping(value = "/find/{username}")
    public Integer getCaregiverIdByUserId(@PathVariable("username") String username) {
        return caregiverService.getCaregiverIdByUserId(username);
    }

    @GetMapping(value = "/patients-in-care/{caregiverId}")
    public List<PatientViewDTO> getPatientsInCare(@PathVariable("caregiverId") String caregiverId) {
        return caregiverService.getPatientsInCare(caregiverId);
    }
}
