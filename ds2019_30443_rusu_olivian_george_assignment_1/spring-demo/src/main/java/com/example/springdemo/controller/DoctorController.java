package com.example.springdemo.controller;


import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/id/{id}")
    public DoctorDTO getById(@PathVariable("id") Integer id) {
        return doctorService.getDoctorById(id);
    }

    @PostMapping(value = "/insert")
    public Integer insertDoctor(@RequestBody DoctorDTO doctorDTO) {
        return doctorService.insertDoctor(doctorDTO);
    }


}
