package com.example.springdemo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
@CrossOrigin
public class SecurityController {

    @GetMapping(value = "/application/login")
    @ResponseBody
    public ResponseEntity<?> currentUserName(Principal principal) {
        final UserDetails currentUser = (UserDetails) ((Authentication) principal).getPrincipal();
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }
}
