package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.entities.User;

public class UserBuilder {

    public UserBuilder() {
    }

    public static User generateEntityFromDTO(UserDTO userDTO) {
        return new User(
          userDTO.getId(),
          userDTO.getUserName(),
          userDTO.getPassWord(),
          userDTO.getRoleType(),
          userDTO.getEnabled()
        );
    }

    public static UserDTO generateDTOFromEntity(User user) {
        return new UserDTO(
                user.getId(),
                user.getUserName(),
                user.getPassWord(),
                user.getRoleType(),
                user.getEnabled()
        );
    }
}
