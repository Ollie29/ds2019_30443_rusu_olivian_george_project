package com.example.springdemo.dto;

import java.util.Date;
import java.util.List;

public class IntakeIntervalDTO {

    private Integer id;
    private String startDate;
    private String endDate;
    private List<MedicationDTO> medicationDTO;

    public IntakeIntervalDTO() {
    }

    public IntakeIntervalDTO(Integer id, String startDate, String endDate, List<MedicationDTO> medicationDTO) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.medicationDTO = medicationDTO;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public List<MedicationDTO> getMedicationDTO() {
        return medicationDTO;
    }

    public void setMedicationDTO(List<MedicationDTO> medicationDTO) {
        this.medicationDTO = medicationDTO;
    }
}
