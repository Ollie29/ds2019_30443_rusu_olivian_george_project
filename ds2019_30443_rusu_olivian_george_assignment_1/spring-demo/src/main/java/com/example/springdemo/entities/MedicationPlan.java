package com.example.springdemo.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "medicationPlan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "intake_interval")
    private Integer intakeInterval;//fetch

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "end_date")
    private String endDate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medicationPlan")
    private List<Medication> medicationList;

    @ManyToOne
    @JoinColumn
    private Patient patient;

    public MedicationPlan() {
    }

    public MedicationPlan(Integer intakeInterval, String startDate, String endDate) {
        this.intakeInterval = intakeInterval;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public MedicationPlan(Integer id, String startDate, String endDate, Integer intakeInterval) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.intakeInterval = intakeInterval;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public Integer getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(Integer intakeInterval) {
        this.intakeInterval = intakeInterval;
    }
}
