package com.example.springdemo.controller;


import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.services.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication/plan")
public class MedicationPlanController {

    private MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @PutMapping(value = "/add/intake/{id}")
    public MedicationPlanDTO addIntakeIntervalToMedicationPlan(@PathVariable("id") Integer id){
        return null;
        //TODO
    }

    @GetMapping(value = "patient/{id}")
    public List<MedicationPlanDTO> getMedPlanByPatientId(@PathVariable("id") Integer id) {
      return medicationPlanService.getAllMedPlans(id);
    }

}
