package com.example.springdemo.controller;


import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/admin/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public Integer insertUser(@RequestBody UserDTO userDTO){
        return userService.insertUser(userDTO);
    }

    @GetMapping(value = "/{username}")
    public Integer getLoggedUserId(@PathVariable("username") String username) {
        return userService.getUserIdByUserName(username);
    }
}
