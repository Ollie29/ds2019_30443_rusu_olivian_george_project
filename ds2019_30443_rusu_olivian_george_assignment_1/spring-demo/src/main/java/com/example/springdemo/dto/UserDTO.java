package com.example.springdemo.dto;

import com.example.springdemo.entities.enums.RoleType;

public class UserDTO {

    private Integer id;
    private String userName;
    private String passWord;
    private RoleType roleType;
    private Boolean enabled;

    public UserDTO() {
    }

    public UserDTO(Integer id, String userName, String passWord, RoleType roleType, Boolean enabled) {
        this.id = id;
        this.userName = userName;
        this.passWord = passWord;
        this.roleType = roleType;
        this.enabled = enabled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
