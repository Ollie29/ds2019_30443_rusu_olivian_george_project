package com.example.springdemo.repositories;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {

    @Query(value = "SELECT caregiver.id FROM caregiver WHERE caregiver.user_id = ?1", nativeQuery = true)
    Integer findCaregiverIdUserId(int userId);

}
