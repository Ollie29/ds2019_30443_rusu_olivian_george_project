package com.example.springdemo.dto;

import com.example.springdemo.entities.enums.DoctorType;

public class DoctorDTO {

    private Integer id;
    private String firstName;
    private String lastName;
    private DoctorType doctorType;

    public DoctorDTO() {
    }

    public DoctorDTO(Integer id, String firstName, String lastName, DoctorType doctorType) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.doctorType = doctorType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }
}
