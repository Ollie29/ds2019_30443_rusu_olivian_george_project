package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.IntakeIntervalDTO;
import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.MedicationPlan;

import java.util.List;
import java.util.stream.Collectors;

public class MedicationPlanBuilder {

    public MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medPlan, List<Medication> meds) {
        List<MedicationDTO> dtos = meds.stream().map(MedicationBuilder::generateDTOFromEntity).collect(Collectors.toList());

        return new MedicationPlanDTO(
                medPlan.getId(),
                medPlan.getStartDate(),
                medPlan.getEndDate(),
                dtos,
                medPlan.getIntakeInterval()
        );
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(
                medicationPlanDTO.getId(),
                medicationPlanDTO.getStartDate(),
                medicationPlanDTO.getEndDate()
        );
    }
}
