package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.dto.builders.MedicationBuilder;
import com.example.springdemo.dto.builders.MedicationPlanBuilder;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    private MedicationPlanRepository medicationPlanRepository;
 //   private IntakeIntervalService intakeIntervalService;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

//    public MedicationPlan addIntakeIntervalToMedicationPlan(Integer medID, Integer medicationPlanID, IntakeIntervalDTO intakeIntervalDTO) {
//        IntakeInterval intakeInterval = intakeIntervalService.insertIntakeIntervalForMedication(intakeIntervalDTO, medID);
//        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(medicationPlanID);
//        if(!medicationPlan.isPresent()) {
//            throw new ResourceNotFoundException("medication plan", "medication plan id", medicationPlanID);
//        }
//        medicationPlan.get().getIntakeIntervals().add(intakeInterval);
//        intakeInterval.setMedicationPlan(medicationPlan.get());
//
//        medicationPlanRepository.save(medicationPlan.get());
//
//        return medicationPlan.get();
//    }

    public List<MedicationPlanDTO> getAllMedPlans(Integer patientId) {
        List<MedicationPlan> medPlans = medicationPlanRepository.getMedPlanByPatientId(patientId);
        return medPlans.stream()
                .map(x -> MedicationPlanBuilder.generateDTOFromEntity(x, x.getMedicationList()))
                .collect(Collectors.toList());
    }
}
