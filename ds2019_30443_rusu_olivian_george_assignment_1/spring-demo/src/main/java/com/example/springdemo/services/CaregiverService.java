package com.example.springdemo.services;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.PatientViewBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.repositories.UserRepository;
import com.example.springdemo.validators.CaregiverFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private CaregiverRepository caregiverRepository;
    private UserRepository userRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, UserRepository userRepository) {
        this.caregiverRepository = caregiverRepository;
        this.userRepository = userRepository;
    }

    public List<CaregiverDTO> findAll() {
        return caregiverRepository.findAll().stream().map(CaregiverBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public CaregiverDTO findCaregiverById(Integer id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);

        if(!caregiver.isPresent()) {
            throw new ResourceNotFoundException("caregiver", "caregiver id", id);
        }

        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
    }

    public Integer insertCaregiver(CaregiverDTO caregiverDTO) {
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
    }

    public void addPatientToCaregiver(Integer caregiverId, PatientDTO patientDTO) {

        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverId);
        Patient patient = new Patient(patientDTO.getId(), patientDTO.getFirstName(), patientDTO.getLastName(),
                patientDTO.getBirthDate(), patientDTO.getGender(), patientDTO.getAddress());
        if(!caregiver.isPresent()) {
            throw new ResourceNotFoundException("caregiver", "caregiver id", caregiverId);
        }

        patient.setCaregiver(caregiver.get());
        caregiver.get().getPatientsInCare().add(patient);
        caregiverRepository.save(caregiver.get());

    }

    public List<PatientViewDTO> viewAllPatientsFromCaregiver(Integer caregiverID) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverID);
        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverID);
        }
        return caregiver.get().getPatientsInCare().stream().map(PatientViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer updateCaregiver(CaregiverDTO caregiverDTO) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getId());

        if(!caregiver.isPresent()) {
            throw new ResourceNotFoundException("caregiver", "caregiver id", caregiverDTO.getId().toString());
        }

        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
    }

    public void deleteCaregiver(int id) {
        caregiverRepository.deleteById(id);
    }

    public Integer getCaregiverIdByUserId(String username) {
        User user = userRepository.findByUserName(username);
        return caregiverRepository.findCaregiverIdUserId(user.getId());
    }

    public List<PatientViewDTO> getPatientsInCare(String username) {
        User user = userRepository.findByUserName(username);
        Integer caregiverId = caregiverRepository.findCaregiverIdUserId(user.getId());

        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverId);

        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("caregiver", "caregiver id", caregiverId);
        }

        List<Patient> patientsInCare = caregiver.get().getPatientsInCare();
        List<PatientViewDTO> dtos = new ArrayList<>();
        for(Patient p : patientsInCare) {
            dtos.add(PatientViewBuilder.generateDTOFromEntity(p));
        }
        return dtos;
    }
}
