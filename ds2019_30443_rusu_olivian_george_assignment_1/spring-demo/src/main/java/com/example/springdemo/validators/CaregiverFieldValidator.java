package com.example.springdemo.validators;



import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class CaregiverFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(PersonFieldValidator.class);

    private CaregiverFieldValidator(){}

    public static void validateInsertOrUpdate(CaregiverDTO caregiverDTO) {

        List<String> errors = new ArrayList<>();
        if (caregiverDTO == null) {
            errors.add("caregiverDTO is null");
            throw new IncorrectParameterException(CaregiverDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }
}
