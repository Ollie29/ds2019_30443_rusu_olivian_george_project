package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.ItemDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class PatientBuilder {

    public PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient) {
        return new PatientDTO(
                patient.getId(),
                patient.getFirstName(),
                patient.getLastName(),
                patient.getBirthDate(),
                patient.getGender(),
                patient.getAddress()
        );
    }

    public static PatientDTO generateDTOFromEntityWithCaregiver(Patient patient) {
        CaregiverDTO caregiverDTO = new CaregiverDTO(patient.getCaregiver().getId(), patient.getCaregiver().getFirstName(), patient.getCaregiver().getLastName(), patient.getCaregiver().getBirthDate(),
                patient.getCaregiver().getGenderType(), patient.getCaregiver().getAddress());

        return new PatientDTO(
                patient.getId(),
                patient.getFirstName(),
                patient.getLastName(),
                patient.getBirthDate(),
                patient.getGender(),
                patient.getAddress(),
                caregiverDTO
        );
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO) {
        return new Patient(
                patientDTO.getId(),
                patientDTO.getFirstName(),
                patientDTO.getLastName(),
                patientDTO.getBirthDate(),
                patientDTO.getGender(),
                patientDTO.getAddress()
        );
    }
}
