package com.example.springdemo.entities.enums;

public enum GenderType {
    MALE,
    FEMALE,
    UNKNOWN
}
