package com.example.springdemo.repositories;

import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {

    @Query(value = "SELECT * FROM medication_plan WHERE medication_plan.patient_id = ?1", nativeQuery = true)
    List<MedicationPlan> getMedPlanByPatientId(Integer patientId);

}
